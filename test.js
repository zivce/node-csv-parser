let assert = require('assert')
let CSVImageDownloader = require("./index");
const fs = require('fs');
const path = require('path')

describe("CSV Image Downloader", function () {
    const VALID_FILE = 'data.csv'
    const INVALID_FILE = 'data2.csv'
    const VALID_COLUMN = 'IMAGE'
    let columnValues = []

    it("should resolve data csv", function () {
        let result = CSVImageDownloader.readCSV(VALID_FILE);
        assert.doesNotReject(result);
    })

    it("should reject when file doesn't exist", function () {
        let result = CSVImageDownloader.readCSV(INVALID_FILE)
        assert.rejects(result)
    })

    it('should parse image links', function () {
        let promise = CSVImageDownloader.readCSV(VALID_FILE);

        let imageAddedProperly = promise.then((result) => {
            columnValues = CSVImageDownloader.parseColumnLinks(VALID_COLUMN, result)
            console.log("Column values: ", columnValues)
            return isArrayOfLinks(columnValues);
        })

        return imageAddedProperly.then((result) => {
            assert.equal(result, true)
        }).catch(() => {
            assert.fail()
        })
    })

    function isArrayOfLinks(columnValues) {
        return columnValues.map(cell => {
            return cell.indexOf('https') == 0;
        }).reduce((acc, cell) => {
            return acc && cell;
        }, true)
    }

    it('should download images', function () {
        const downloadFolderName = "downloadHere";
        const downloadDir = path.join(__dirname, downloadFolderName);

        CSVImageDownloader.downloadImages(columnValues, downloadFolderName);

        fs.readdir(downloadDir, (err, files) => {
            if(err) {
               return assert.fail(err);
            }

            assert.equal(files.length, 2);
            fs.readFile(path.join(downloadDir, files[0]), (err, data) => {
                if(err) {
                    return assert.fail(err);
                }
                assert.notEqual(data.byteLength, 0);
            })
        }); 
        
        fs.rmdir(downloadDir, { recursive: true }, () => {console.log("Deleted download folder!")})

       
    })

    it("should resize images to 100x80", function () {
        assert.fail();
    })

})
