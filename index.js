const csv = require('csv-parser');
const fs = require('fs');
const https = require('https')
const path = require('path')

exports.readCSV = function (filePath) {
    const results = [];
    const filePromise = new Promise((resolve, reject) => {

        let readStream = fs.createReadStream(filePath)
        readStream.on('error', error => reject(error))

        readStream.pipe(csv())
            .on('data', (row) => {
                results.push(row);
                console.log(results);
            })
            .on('end', () => resolve(results));
    });

    return filePromise.then(links => {
        console.log(links);console.log("links");
        return links;
    })
        .catch(error => reject(error))
}

exports.parseColumnLinks = function (column, data) {
    return data.reduce((acc, row) => {
        const cell = row[column]

        if (cell.indexOf('https') < 0) {
            return acc;
        }
        acc.push(cell)
        return acc;

    }, []);
}

exports.downloadImages = function(links, downloadFolder) {
    links.forEach((link) => {
        const parts = link.split("/");
        const result = parts.pop();
        const downloadDirPath = path.join(__dirname, downloadFolder);

        imagePath = path.join(downloadDirPath, result + Date.now().toString() + ".png");
        console.log(imagePath)
        
        if (!fs.existsSync(downloadDirPath)){
            fs.mkdirSync(downloadDirPath);
        }

        var writeStream = fs.createWriteStream(imagePath);

        writeStream.on('error', function (err) {
            console.error(err);
            return;
        });
        const data = [];

        https.get(link, function (response) {
            response.setEncoding('binary');
            response.on('data', (chunk) =>{
                data.push(Buffer.from(chunk,'binary'))
            }).on('end', () => {
                const buffer = Buffer.concat(data);
                writeStream.write(buffer);
            })
        });
    })



}